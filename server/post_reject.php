<?php
require_once("../server/conf.php");
?>
<!doctype html>
<html>

<head>
	<meta charset="UTF-8" />
	<link href="../asset/theme/default.css" type="text/css" rel="stylesheet" />
	<link href="../asset/theme/<?php echo(THEME); ?>/css/global.css" type="text/css" rel="stylesheet" />
</head>
<body>
	<h1 style="margin-bottom:0.5em;">驳回</h1>
	<p style="margin-bottom:0.5em;">如刷新本页，浏览器会提示您是否重新提交表单。</p>
	<p>如在历史记录中后退或前进至本页，会产生过期问题。</p>
</body>
</html>