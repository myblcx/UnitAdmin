<?php
require_once("../server/conf.php");
set_include_path(APP_ROOT."/server/phpseclib/");
include('Crypt/RSA.php');

// RSA对key解密
$privatekey="-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQC3RCD1pNmr/SByydk23VPi3iqnkIIq0WCIB72j4XazNcUZAsohd4JBmBgc6L6o XeEyquoRBP0EPkrSwK9wW9qWa10vkqarUXDRYesej3prHV+6Zz+KTc6+VUB++XB3gskbF1J6+CCi w6O1hjQa5U7wNzkHTUc44/81JXvfuSM8UwIDAQABAoGAUYaLKvt0oZ/vKWFFsbRvtsKiMvyEC0wt JxZadGa+CSboUSH+jTi+xzNDtsiK2Bc7MPD7Qyr260ZSvsJcyRzzdanwewenUMLXAL1JOaZhxQ2+ tcbWDiX6aByL5lkGu4cxNGpEGoa34fo7bFMMzqpAjgMqTIocJWMxMIdRCkzwfdECQQDemsSLSST/ TD4gd/CB9m/NrdPWzRgyneHvKCT17g777ILzW3JsGrGqiZzWqMxF/ynN32XwM/FT7V00dAnngeGN AkEA0sKM0RQTvt7pKn8vHCZUNsbSMkgT52DbWrKFEUA660808BmflvpuwKXZZ+8vKN0F73e76eF1 95eUdtauy+XtXwJBAKl+tLrNpfMSLZfxW1rJtxWoDs3Wel9IIhlEuufbLOObkZYVAknYBYGxqI82 FdwSTtVoDalZE57w9HAVDtmM5p0CQBZoiQBR2ieZG8Fg9GlRyfJpAUBHWZZoPepOwMcsxRbvvPkq QEWVKuFgwNTEIYd+uHrViC09w4UnoKlh+gPD1pECQB78s8sG3V9Et+2DnwDOJ4prZXfOikV2Hlf/ BTwQp7L0iMErKSCV07Ek3/Pbjg+VU431wo8ZHvBOydFBfBkAS9A=
----END RSA PRIVATE KEY-----";
function publicKeyToHex($privatekey) {
	$rsa = new Crypt_RSA();
	$rsa->loadKey($privatekey);
	$raw = $rsa->getPublicKey(CRYPT_RSA_PUBLIC_FORMAT_RAW);
	return $raw['n']->toHex();
}

function decrypt($privatekey, $encrypted) {
	$rsa = new Crypt_RSA();
	$encrypted=pack('H*', $encrypted);
	$rsa->loadKey($privatekey);
	$rsa->setEncryptionMode(CRYPT_RSA_ENCRYPTION_PKCS1);
	return $rsa->decrypt($encrypted);
}

$key = decrypt($privatekey, $_POST["key"]);
$iv='1234567812345678';

$card_no = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, base64_decode($_POST['card_no']), MCRYPT_MODE_CBC, $iv);
$user_id = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, base64_decode($_POST['user_id']), MCRYPT_MODE_CBC, $iv);


?>
<!doctype html>
<html>
	<head>
		<meta charset="UTF-8" />
		<title></title>
		<link href="../asset/theme/default.css" type="text/css" rel="stylesheet" />
		<link href="../asset/theme/<?php echo(THEME); ?>/css/global.css" type="text/css" rel="stylesheet" />
	</head>
	<body>
	<div class="cmpt-workGround-header">
		<h1>数据加密传递成功</h1>
	</div>
	<div class="cmpt-dataInput-formPanel">
		<div class="formPanel-header">
			<h1><span class="icon-form"></span>传递的密文</h1>
		</div>
		<div class="formPanel-body">
			<table>
				<tr>
					<th width="100">AES随机Key：</th>
					<td><?php echo $_POST["key"]; ?></td>
				</tr>
				<tr>
					<th>卡号：</th>
					<td><?php echo $_POST["card_no"]; ?></td>
				</tr>
				<tr>
					<th>身份证号：</th>
					<td><?php echo $_POST["user_id"]; ?></td>
				</tr>
			</table>
		</div>
	</div>
	<div class="cmpt-dataInput-formPanel">
		<div class="formPanel-header">
			<h1><span class="icon-form"></span>解密后的原文</h1>
		</div>
		<div class="formPanel-body">
			<table>
				<tr>
					<th width="100">AES随机Key：</th>
					<td><?php echo $key; ?></td>
				</tr>
				<tr>
					<th>卡号：</th>
					<td><?php echo urldecode($card_no); ?></td>
				</tr>
				<tr>
					<th>身份证号：</th>
					<td><?php echo urldecode($user_id); ?></td>
				</tr>
			</table>
		</div>
	</div>
	</body>
</html>