<?php
define("WEB_ROOT", "/admin");
define("APP_ROOT", $_SERVER["DOCUMENT_ROOT"]."/".WEB_ROOT);

define("THEME", "steelblue");
$logo_file = array(
	"light"=>"logo_black.png",
	"steelblue"=>"logo_white.png"
);