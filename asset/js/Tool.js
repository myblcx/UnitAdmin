;
(function(window, jQuery){
	var docElm = document.documentElement;
	window.jqBody = window.jqBody || jQuery('body');
	window.jqWin = window.jqWin || jQuery(window);
	window.jqDocElm = window.jqDocElm || jQuery(document.documentElement);

	var Tool = {
		refloat: function(pElm, pIsFixed){
			var isFixed = pIsFixed;
			if( !window.XMLHttpRequest ) isFixed = false;
			pElm.css({
				left: (jqWin.width() - pElm.width())/2,
				top: (jqWin.height() - pElm.height())/2 + (!isFixed? docElm.scrollTop : 0)
			});
		},

		serialize: function(pOJSON){
			var strResult = '';
			for( var key in pOJSON){
				strResult = strResult + '&' + key + '=' + pOJSON[key];
			}
			return strResult.replace('&', '');
		},

		getParameter: function(){
			var strSearch = window.location.search.replace('?', ''),
				oParameters = {},
				arrSearch = strSearch.split('&') || [strSearch];
			if( arrSearch ){
				for(var i=0, ln=arrSearch.length; i<ln; i++){
					var strPair = arrSearch[i], arrPair = null;
					if( strPair.indexOf('=') > -1 ){
						arrPair = strPair.split('=');
					}
					if( arrPair ){
						if( oParameters[arrPair[0]] ){
							oParameters[arrPair[0]] = oParameters[arrPair[0]] + ',' + arrPair[1];
						}else{
							oParameters[arrPair[0]] = arrPair[1];
						}
					}
				}
			}
			return oParameters;
		},

		setParameter: function(pStrName, pStrValue){
			var oParameters = Tool.getParameter();
			if( arguments.length == 1){
				for(var item in pStrName ){
					oParameters[item] = pStrName[item];
				}
			}else{
				oParameters[pStrName] = pStrValue;
			}
			return Tool.serialize(oParameters);
		}
	};

	window.Tool = Tool;
})(window, jQuery);