
;(function(window, jQuery){
	/**
	 * 文本格式化
	 * 支持格式化文本为
	 *     银行卡卡号
	 *     货币字符串
	 *     日期格式
	 * @constructor
	 */
	var TextFormat = {
		cardNo: function(pText){
			if( pText.length == 0 ) return '';
			pText = pText.replace(/\s/g, '');
			var arr = pText.match(/\d{1,4}/g);
			return arr.join('&nbsp;&nbsp;');
		}
	};

	window.TextFormat = TextFormat;
})(window, jQuery);