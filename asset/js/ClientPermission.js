
;(function(window, jQuery){
	/**
	 * 客户端操作许可
	 * @static {Object}
	 */
	var ClientPermission = {
		/**
		 * 禁止复制
		 * @param elmElements
		 */
		copyForbidden: function(elmElements){
			elmElements.on('copy', function(event){
				event.preventDefault();
			});
		}
	};

	window.ClientPermission = ClientPermission;
})(window, jQuery);