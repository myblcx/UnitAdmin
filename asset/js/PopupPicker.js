// 弹窗选择器
;(function(window, jQuery){
	var PopupPicker = function(pElmPopupBtn, pOutputs){
		this._outputs = pOutputs;

		pElmPopupBtn.on('click', function(){
			window.parent.openIfmDialog('双击选择条目', pElmPopupBtn.attr('data-url'));
		});
	};
	PopupPicker.prototype = {
		achieveData: function(pData){
			var outputs = this._outputs;
			for(var i= 0,ln=outputs.length;i<ln; i++){
				var elmOutput = outputs[i];
				elmOutput.val(pData[elmOutput.attr('name')]);
				elmOutput.blur(); // 触发验证
			}
			window.parent.closeIfmDialog();
		}
	};
	PopupPicker.prototype.constructor = PopupPicker;

	PopupPicker.createDataTable = function(){

	};

	window.PopupPicker = PopupPicker;
})(window, jQuery);
