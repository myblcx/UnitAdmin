;
window.elmWin = window.elmWin || jQuery(window);
window.elmBody = window.elmBody || jQuery('body');
(function(window, jQuery){
	var PreloadingPage = {
		beginLoad: function(){
			var elmMask = jQuery('<div class="cmpt-mask"></div>'),
				elmPreload = jQuery('<div class="cmpt-fullPreloading-32"></div>');
			elmBody.append(elmMask);
			elmMask.height(elmWin.height());
			elmBody.append(elmPreload);
			Tool.refloat(elmPreload);
		}
	};
	window.PreloadingPage = PreloadingPage;
})(window, jQuery);