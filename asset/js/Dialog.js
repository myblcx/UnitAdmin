;
(function (window, jQuery) {
	window.jqBody = window.jqBody || jQuery('body');
	window.jqWin = window.jqWin || jQuery(window);
	window.elmDocElement = window.elmDocElement || jQuery(document.documentElement);

	/*基础对话框对象，用该对象扩展*/
	var BasicDialog = function(pElmContent, pStrTitle, pFnCallback_OnClose, pIntWidth, pInstName){
		var oDC = createTemplate(),
			jqDialogClose = oDC.closeButton,
			jqDialogBody = oDC.body,
			jqDialogTitle = oDC.title,
			that = this;
		// 标题
		jqDialogTitle.text(pStrTitle);
		// 内容
		jqDialogBody.append(pElmContent);
		// 事件
		// 关闭
		var fnOnCloseClick = function(){
			if( pFnCallback_OnClose ) pFnCallback_OnClose(oDC);
			that.destroy();
		};
		jqDialogClose.on('click', fnOnCloseClick);
		// 添加到DOM
		if( pIntWidth ) oDC.dialog.width(pIntWidth);
		jqBody.append(oDC.dialog);
		// 挂载
		this._fnOnCloseClick = fnOnCloseClick;
		this._oDC = oDC;
		this._instName = pInstName;
	};
	BasicDialog.prototype = {
		// 添加操作栏
		addOptions: function(pFnCallback_OnConfirm){
			var oDC = this._oDC,
				jqOptions = oDC.options,
				jqConfirm = jqOptions.find('[role="confirm"]'),
				jqCancel = jqOptions.find('[role="cancel"]'),
				that = this;
			// 追加操作栏
			oDC.body.append(jqOptions);
			// 事件
			var fnOnConfirmClick = function(){
				if( pFnCallback_OnConfirm ) pFnCallback_OnConfirm(oDC);
				that.destroy();
			};
			jqCancel.on('click', this._fnOnCloseClick);
			jqConfirm.on('click', fnOnConfirmClick);
			// 挂载
			this._fnCallback_onConfirm = pFnCallback_OnConfirm;
			this._fnOnConfirmClick = fnOnConfirmClick;
			this._jqConfirm = jqConfirm;
			this._jqCancel = jqCancel;
		},
		/**
		 * 销毁*/
		destroy: function(){
			var oDC = this._oDC,
				jqDialogClose = oDC.closeButton;
			elmDocElement.removeClass('screenFixed');

			jqDialogClose.off('click', this._fnOnCloseClick);
			if( this._jqConfirm ) this._jqConfirm.off('click', this._fnOnConfirmClick);
			if( this._jqCancel ) this._jqCancel.off('click', this._fnOnCloseClick);
			oDC.mask.remove();
			oDC.dialog.remove();
			this.removeInstance();
			for(var item in this) delete this[item];
		},
		// 显示
		show: function(){
			var oDC = this._oDC,
				jqMask = oDC.mask;
			// 定位
			Tool.refloat(oDC.dialog, true);
			// 记录初始的滚动高度
			this._scrollTop = document.documentElement.scrollTop;
			// IE7 BUG html标签上添加类名，去除滚动条
			elmDocElement.addClass('screenFixed');
			// 添加遮罩
			jqBody.append(jqMask);
			// 取更高的高度
			jqMask.height(Math.max(jqBody.height(), jqWin.height()));
			this.setInstance();
			// IE7 BUG，还原到初始滚动高度
			window.scroll(0, this._scrollTop);
		},
		// 获取组件
		getDC: function(){
			return this._oDC;
		},
		// 在Dialog对象上挂载实例对象，提供对给对外引用
		setInstance: function(){
			if(this._instName){
				if(Dialog[this._instName]){
					throw new Error('不能重复使用实例名');
					return;
				}
				Dialog[this._instName] = this;
			}
		},
		removeInstance: function(){
			if(this._instName) delete Dialog[this._instName];
		}
	};
	/**
	 * 自定义对话框，静态调用
	 * 举例：
	 * <pre>
	 *     Dialog.alert('消息提示内容', function(){ console.log('关闭'); });
	 * </pre>*/
	var Dialog = {
		/**
		 * 警告对话框*/
		alert: function(pStrMsg, pStrTitle, pFnOnClose){
			var elmContent = jQuery('<div class="dialog-alert-msg">'+pStrMsg+'</div>'),
				strTitle = pStrTitle || '警告';
			var alertDialog = new BasicDialog(elmContent, strTitle, pFnOnClose, 200);
			alertDialog.show();
		},
		confirm: function(pStrMsg, pFnOnConfirm, pFnOnCancel){
			var elmContent = jQuery('<div class="msgBox">'+pStrMsg+'</div>'),
				intWidth = getMsgWidth(pStrMsg);
			intWidth = intWidth < 200 ? 200 : intWidth;
			var confirmDialog = new BasicDialog(elmContent, '询问', pFnOnCancel, intWidth);
			confirmDialog.addOptions(pFnOnConfirm);
			confirmDialog.show();
		},
		iframe: function(pStrTitle, pSrc, pOpts, pInstName){
			var options = {
					width: 600,
					onClose: function(){},
					onIfmLoad: function(){}
				},
				jqIfm;
			jQuery.extend(options, pOpts);
			if( pSrc.constructor == jQuery ) {
				// 如果形参是iframe
				jqIfm = pSrc;
			}else jqIfm = jQuery('<iframe src="'+pSrc+'"></iframe>');
			// 统一设置属性
			jqIfm.attr({
				'class': 'hide',
				'frameborder': '0',
				'scrolling': 'yes',
				'horizontalscrolling': 'no',
				'verticalscrolling': 'yes'
			});
			var iframeDialog = new BasicDialog(jqIfm, pStrTitle, function(){
				jqIfm.off('load');
				options.onClose();
			}, options.width, pInstName);
			var oDC = iframeDialog.getDC(),
				jqDialogBody = oDC.body;
			// 预加载
			var jqPreloading = jQuery('<div class="ifmLoading cmpt-preloading-32"></div>');
			jqDialogBody.append(jqPreloading);
			jqIfm.height(0);
			jqIfm.on('load', function(){
				options.onIfmLoad();
				setTimeout(function(){
					jqPreloading.remove();
					jqIfm.removeClass('hide');
					var intMaxHeight = (jqWin.height() - 50)*0.8,
						intContentHeight = jqIfm.contents()[0].documentElement.scrollHeight;
					jqIfm.height(Math.min(intMaxHeight, intContentHeight));
					Tool.refloat(oDC.dialog, true);
				}, 200);
			});
			iframeDialog.show();
			return iframeDialog;
		}
	};
	function createTemplate(){
		var randNum = new Date().getTime(),
			dialogId = 'Dialog_' + randNum,
			headerId = 'DialogHeader_' + randNum,
			titleId = 'DialogTitle_' + randNum,
			bodyId = 'DialogBody_' + randNum,
			closeButtonId = 'DialogButtonClose_' + randNum,
			confirmButtonId = 'DialogConfirm_' + randNum,
			optionsId = 'DialogOptions_' + randNum,
			maskId = 'Mask_' + randNum,
			dialogHTML = '<div id="' + dialogId + '" class="cmpt-dialog">' +
				'<div class="dialog-content">' +
					'<div id="' + headerId + '" class="dialog-head">' +
						'<span class="icon"></span>'+
						'<div class="title" id="' + titleId + '"></div>' +
						'<span class="close" role="cancel" id="' + closeButtonId + '"></span>' +
					'</div>' +
					'<div class="dialog-body" id="' + bodyId + '"></div>' +
				'</div>',
			optionsHTML = '<div id="'+optionsId+'" class="options"><span role="confirm" class="button btn-color-2a6aaa btn-h-25 btn-bdra-1">确定</span><span role="cancel" class="button btn-color-2a6aaa btn-h-25 btn-bdra-1">取消</span></div>',
			maskHTML = '<div id="' + maskId + '" class="cmpt-mask"></div>';
		var jqDialog = jQuery(dialogHTML),
			jqOptions = jQuery(optionsHTML),
			jqMask = jQuery(maskHTML);
		return {
			dialog : jqDialog,
			header: jqDialog.find('#' + headerId),
			title : jqDialog.find('#'+titleId),
			body : jqDialog.find('#'+bodyId),
			closeButton : jqDialog.find('#'+closeButtonId),
			mask: jqMask,
			options : jqOptions,
			width: 200
		};
	}
	function getMsgWidth(pStrContent){
		var intLn = pStrContent.length;
		if(intLn <= 33 ) return (12*intLn+20);
		else return (12*33+10);
	}
	window.Dialog = Dialog;
})(window, jQuery);