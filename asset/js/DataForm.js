;(function(window, jQuery){
	var DataForm = function(pElmForm, pElmSubmit, pFv, settings){
		if( !pFv) throw new Error('实例错误');
		if( pFv.constructor != FormValidator ) throw new Error('DataForm:FormValidator实例不正确');
		this._elmForm = pElmForm;
		this._fv = pFv;
		this._elmSubmitContainer = pElmSubmit.parent();
		var me = this,
			options = {
				commentName : 'comment', // 驳回理由的name
				onBeforeSubmit: function(){ return true; }
			};
		jQuery.extend(options, settings);
		this._options = options;
		pElmSubmit.on('click', function(){
			var elmSubmit = jQuery(this),
				attrVerify = elmSubmit.attr('data-verify'),
				attrAction = elmSubmit.attr('data-action'),
				attrPrompt = elmSubmit.attr('data-prompt');
			// 验证
			if( attrVerify )
				if( !me.verifyAll() ) return;
			// 改变action
			if( attrAction )
				me.changeFormAction(attrAction);
			// 提交前填写额外信息
			if( attrPrompt )
				if( !me.prompt(attrPrompt) ) return;
			// 其他
			if( !options.onBeforeSubmit() ) return;
			// 提交
			PreloadingPage.beginLoad();
			pElmForm.submit();

		});
	};

	DataForm.prototype = {
		verifyAll: function(){
			var elmContainer = this._elmSubmitContainer,
				fv = this._fv,
				elmErr = elmContainer.find('.msg-err'),
				isAllVerify = fv.verifyAll();
			if( !isAllVerify ){
				if( elmErr.length == 0 ) {
					elmErr = jQuery('<span class="msg-err"></span>');
					elmContainer.prepend(elmErr);
				}
				elmErr.text('表单输入有误，请检查');
			}
			return isAllVerify;
		},
		changeFormAction: function(pStrAction){
			this._elmForm.attr('action', pStrAction);
		},
		prompt: function(pStrPromptMsg){
			var elmContainer = this._elmSubmitContainer,
				strValue = window.prompt(pStrPromptMsg, ''),
				elmErr = elmContainer.find('.msg-err');
			if( strValue != null ){
				if( jQuery.trim(strValue).length == 0 ){
					if( elmErr.length == 0 ){
						elmErr = jQuery('<span class="msg-err"></span>');
						elmContainer.prepend(elmErr);
					}
					elmErr.text('请填写驳回理由');
					strValue = null;
				}else{
					var elmComment = this._elmForm.find('[name="'+this._options.commentName+'"]');
					if( elmComment.length == 0 ) elmComment = jQuery('<input type="hidden" name="'+this._options.commentName+'" />');
					elmComment.val(strValue);
					this._elmForm.append(elmComment);
				}
			}
			return strValue
		}
	};


	window.DataForm = DataForm;
})(window, jQuery);