<?php
require_once("server/conf.php");
?>
<!DOCTYPE html>
<html>
<head>
	<script>
		// 如果登录页被嵌套在iframe中，将重定向父页面
		if(window.parent != window.self)
			window.parent.location.replace(window.location.href);
	</script>
	<meta charset="UTF-8" />
	<title>Admin登录</title>
	<link href="asset/theme/default.css" type="text/css" rel="stylesheet" />
	<link href="asset/theme/<?php echo(THEME);?>/css/global.css" type="text/css" rel="stylesheet" />
</head>
<body>
<div class="cmpt-page-header">
	<div style="margin:0 auto;width:600px;">
		<div class="cmpt-logo"><img src="asset/image/ft/<?php echo($logo_file[THEME]);?>" alt="华势作业系统平台logo"/></div>
	</div>
</div>
<div class="cmpt-login">
	<div class="cmpt-dataInput-formPanel">
		<div class="formPanel-header">
			<h1><span class="icon-form"></span>用户登录</h1>
		</div>
		<form id="Form" action="index.php" method="POST">
		<div class="formPanel-serverErrorMsg">
			<p>验证码错误</p>
		</div>
		<div class="formPanel-body">
			<table>
				<tr>
					<th width="100">账号：</th>
					<td>
						<input name="username"
							type="text"
							required="required"
							class="field-text grid_5"/>
					</td>
				</tr>
				<tr>
					<th>密码：</th>
					<td>
						<input name="password"
							type="password"
							required="required"
							class="field-text grid_5"/>
					</td>
				</tr>
				<tr>
					<th>验证码：</th>
					<td><input type="text" name="verify_code" required="required" class="field-text grid_3" />
						<img src="asset/image/ft/verify_code.jpg" />
					</td>
				</tr>
			</table>
		</div>
		<div class="formPanel-footer">
			<span id="Submit" data-verify="verify" class="button button-positive">登录</span>
		</div>
		</form>
	</div><!--/formPanel-->
</div>
<script src="asset/lib/jquery.js" ></script>
<script src="asset/lib/sec/md5.js" ></script>
<script src="asset/js/admin.mini.js" ></script>
<script>
	var elmForm = jQuery('#Form'),
			elmPassword = elmForm.find('[type="password"]'),
			elmSubmit = jQuery('#Submit');
	var fv = new FormValidator(elmForm);
	var df = new DataForm(elmForm, elmSubmit, fv, {
		onBeforeSubmit: function(){
			elmPassword.val(CryptoJS.MD5(elmPassword.val()).toString());
			return true;
		}
	});
</script>
</body>
</html>