<?php
	require_once("server/conf.php");
?>
<!DOCTYPE HTML>
<html>
<head>
	<script>
		// 确保本页不被嵌套在iframe内
		if(window.parent != window.self)
			window.parent.location.replace(window.location.href);
	</script>
	<meta charset="UTF-8" />
	<title>UnitAdmin</title>
	<link href="asset/theme/default.css" type="text/css" rel="stylesheet" />
	<link href="asset/theme/<?php echo(THEME);?>/css/global.css" type="text/css" rel="stylesheet" />
</head>
<body>
	<div class="cmpt-page-header">
		<div class="cmpt-logo"></div>
		<div class="cmpt-headerOperations">
			<div class="operation" id="UserOperation">
				<span class="icon-setting"></span>
				<div class="cmpt-userPanel">
					<ul>
						<li><a href="#">注销</a></li>
						<li><a href="#">修改密码</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="cmpt-page-sidebar" id="Sidebar">
		<div class="cmpt-breadLine">
			<div class="cmpt-userActive">用户名</div>
		</div>
		<div class="cmpt-globalNavigation">
			<ul id="GlobalNav">
				<!--一级菜单-->
				<li class="display">
					<div>
						<span class="ico-arr-r"></span>
						<span class="text">演示模板</span>
					</div>
					<ul>
						<li>
							<a target="work_ground" href="repo/template/data_output_list.php"><span class="text">数据列表</span></a>
						</li>
						<li>
							<a target="work_ground" href="repo/template/data_input_form.php"><span class="text">表单</span></a>
						</li>
						<li>
							<a target="work_ground" href="repo/template/data_crypt_input_form.php"><span class="text">表单数据加密</span></a>
						</li>
						<li>
							<a target="work_ground" href="login.php"><span class="text">登录页</span></a>
						</li>
					</ul>
				</li>
				<!--一级菜单-->
				<li class="display">
					<div>
						<span class="ico-arr-r"></span>
						<span class="text">服务器错误</span>
					</div>
					<!--二级菜单-->
					<ul>
						<li>
							<a target="work_ground" href="repo/error/404.php"><span class="text">404</span></a>
						</li>
					</ul>
				</li>
				<!--一级菜单-->
				<li class="display">
					<div>
						<span class="ico-arr-r"></span>
						<span class="text">商户管理</span>
					</div>
					<!--二级菜单-->
					<ul>
						<li>
							<div>
								<span class="ico-arr-r"></span>
								<span class="text">商户信息变更</span>
							</div>
							<ul>
								<li>
									<a href="#">
										<span class="text">变更申请</span>
									</a>
								</li>
								<li>
									<a href="#">
										<span class="text">变更初审</span>
									</a>
								</li>
								<li>
									<a href="#">
										<span class="text">变更终审</span>
									</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="#">商户文件上传</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="cmpt-calendar">
			<div class="calendar-header">
				<h1>2013</h1>
			</div>
			<div id="Calendar" class="calendar-body"></div>
		</div>
	</div>
	<div class="cmpt-page-main" id="Main">
		<div class="cmpt-breadLine">
			<div class="cmpt-preOperations">
				<a title="隐藏侧栏" id="ToggleSidebar"><span class="icon-pushLeft"></span></a>
			</div>
			<div class="cmpt-breadOperations">
				<a target="work_ground" href="repo/dashboard.php"><span class="icon-anno"></span><span>系统公告</span></a>
				<a id="ReloadWorkGround"><span class="icon-refresh"></span><span>刷新</span></a>
				<a id="HistoryBack"><span class="icon-back"></span><span>后退</span></a>
			</div>
		</div>
		<div class="cmpt-workGround">
			<div class="workGround-body">
				<iframe id="WorkGround"
					name="work_ground"
					scrolling="no"
					frameborder="0"></iframe>
			</div>
		</div>
	</div>

	<script src="asset/lib/jquery.js"></script>
	<script src="asset/js/calendar.js"></script>
	<script src="asset/js/admin.mini.js"></script>
	<script>
		(function(window, jQuery){
			var elmWin = jQuery(window),
					elmDoc = jQuery(document);
			// 日历
			document.getElementById('Calendar').innerHTML = (new MBClendar(6, 2013)).toHTML();

			// 全局导航
			jQuery('#GlobalNav>li>div').on('click', function(){
				jQuery(this).parent().toggleClass('display');
			});

			// 用户操作面板
			var elmUserOperation = jQuery('#UserOperation');
			elmUserOperation.on('click', function(event){
				event.stopPropagation();
				if( elmUserOperation.hasClass('display')){
					elmUserOperation.removeClass('display');
					elmDoc.off('click.toggleUserPanel');
				}else{
					elmUserOperation.addClass('display');
					elmDoc.on('click.toggleUserPanel', function(){
						elmUserOperation.trigger('click');
					});
				}
			});

			// 工作区
			var elmWorkgroundIfm = jQuery('#WorkGround');
			/**
			 * 重置iframe高度，在下面时刻调用：
			 * 1、iframe内部需要改变高度时
			 */
			window.resizeWrokGround = function(){
				elmWorkgroundIfm.height(0);
				elmWorkgroundIfm.height(elmWorkgroundIfm.contents()[0].documentElement.scrollHeight);
			};
			/**
			 * 工作区是否在加载中
			 * @type {Boolean}
			 */
			var isWorkGroundLoading = false;
			/**
			 * 工作区window对象的引用，用于窗体之间通信
			 * @type {Window}
			 */
			window.winWorkGround = function(){
				return elmWorkgroundIfm[0].contentWindow;
			};

			// 初始化工作区
			elmWorkgroundIfm.attr('src', 'repo/dashboard.php');
			// 工作区事件
			elmWorkgroundIfm.on('load', function(){
				resizeWrokGround();
			});

			// 刷新工作区
			jQuery('#ReloadWorkGround').on('click', function(){
				if( isWorkGroundLoading ) return;
				// 刷新
				elmWorkgroundIfm.contents()[0].location.reload();
			});
			// 后退
			jQuery('#HistoryBack').on('click', function(){
				window.history.back();
			});

			// 封装弹窗方法，供不同子窗体之间调用
			var IFM_DIALOG_NAME = 'popupWin';
			window.openIfmDialog = function(pStrTitle, pSrc, pOpts){
				Dialog.iframe(pStrTitle, pSrc, pOpts, IFM_DIALOG_NAME);
			};
			window.closeIfmDialog = function(){
				Dialog[IFM_DIALOG_NAME].destroy();
			};

			// 侧栏Toggle
			var elmToggleSidebar = jQuery('#ToggleSidebar'),
					elmMain = jQuery('#Main'),
					strMarginLeft = elmMain.css('marginLeft'),
					elmSidebar = jQuery('#Sidebar');
			elmToggleSidebar.on('click', function(){
				var elmIcon = elmToggleSidebar.find('span');
				if( elmIcon.hasClass('icon-pushLeft') ){
					// 隐藏
					elmIcon.removeClass('icon-pushLeft');
					elmIcon.addClass('icon-pushRight');
					elmSidebar.hide();
					elmMain.css('marginLeft',0);
					elmToggleSidebar.attr('title', '展开侧栏');
				}else{
					elmIcon.removeClass('icon-pushRight');
					elmIcon.addClass('icon-pushLeft');
					elmSidebar.show();
					elmMain.css('marginLeft',strMarginLeft);
					elmToggleSidebar.attr('title', '隐藏侧栏');
				}
			});
		})(window, jQuery);

		console.log(navigator.mimeTypes);
	</script>
</body>
</html>