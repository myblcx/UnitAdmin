<?php
require_once("../../server/conf.php");
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF8" />
	<title></title>
	<link href="../../asset/theme/default.css" type="text/css" rel="stylesheet" />
	<link href="../../asset/theme/<?php echo(THEME);?>/css/global.css" type="text/css" rel="stylesheet" />
</head>
<body>
<div id="PageHeader" class="cmpt-page-header" style="display:none;">
	<div class="cmpt-logo"><img src="../../asset/image/ft/<?php echo($logo_file[THEME]);?>" alt="华势作业系统平台logo"/></div>
</div>
	<script>
		if(window.parent == window.self){
			document.getElementById('PageHeader').style.display = 'block';
		}
	</script>
	<div class="page-serverError">
		<h1>404</h1>
		<p>你正在寻找不存在的页面。</p>
		<p><a class="button button-positive" href="../../">首页</a></p>
	</div>
</body>
</html>