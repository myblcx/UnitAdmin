<?php
require_once("../../server/conf.php");
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title></title>
	<link href="../../asset/theme/default.css" type="text/css" rel="stylesheet" />
	<link href="../../asset/theme/<?php echo(THEME);?>/css/global.css" type="text/css" rel="stylesheet" />
	<link href="../../asset/theme/<?php echo(THEME);?>/css/dialog.css" type="text/css" rel="stylesheet" />
</head>
<body class="page-dialog">
<!--cmpt-dataOutput-table-dblPicker 是可以双击选取行的表格样式-->
<div class="cmpt-dataOutput-table cmpt-dataOutput-table-dblPicker" id="DataOutput">
	<form action="dialog.pickup_list.php" method="GET" role="queryForm">
	<div class="dataTable-simpleFilter">
		<p>
			<input class="field-text" type="text" placeholder="卡号" />
			<input class="field-text" type="text" placeholder="卡号" /></p>
		<p>
			<input class="field-text" type="text" placeholder="账户名" />
			<input class="field-text" type="text" placeholder="卡号" />
			<span class="button button-query" role="querySubmit">查询</span>
		</p>
	</div>
	</form>
	<div class="dataTable-body">
		<table class="dataList" role="dataList">
			<thead>
			<tr>
				<th><span>商户编号</span></th>
				<th><span>终端编号</span></th>
				<th><span>&nbsp;</span></th>
			</tr>
			</thead>
			<tbody id="DataListBody">
			<tr data="merPaper:001,pink:蓝色">
				<td>0001</td>
				<td>a1900001</td>
				<td>&nbsp;</td>
			</tr>
			<tr data="merPaper:002,pink:绿色">
				<td>0002</td>
				<td>a1900002</td>
				<td>&nbsp;</td>
			</tr>
			<tr data="merPaper:003,pink:红色">
				<td>0003</td>
				<td>a1900003</td>
				<td>&nbsp;</td>
			</tr>
			</tbody>
		</table>
	</div>
	<div class="dataTable-footer clearfix">
		<div role="pagination" class="pagination">
			<a href="http://www.baidu.com">&lt;</a>
			<a href="#">1</a>
			<span>...</span>
			<a href="#">11</a>
			<a class="selected">12</a>
			<a href="#">13</a>
			<span>...</span>
			<a href="#">23</a>
			<a>&gt;</a>
		</div>
	</div>
</div>

<script src="../../asset/lib/jquery.js"></script>
<script src="../../asset/js/admin.mini.js"></script>
<script>
		var dl = new DataList(jQuery('#DataOutput'));
		jQuery('#DataListBody').on('dblclick', 'tr', function(){
			var arrDatas = jQuery(this).attr('data').split(',');
			var oData = {};
			for(var i=0,ln=arrDatas.length;i<ln;i++){
				var data = arrDatas[i].split(':');
				oData[data[0]] = data[1];
			}
			window.parent.winWorkGround().popupPicker.achieveData(oData);
		});

</script>
</body>
</html>