<?php
require_once("../../server/conf.php");
?>
<!doctype html>
<html>
	<head>
		<meta charset="UTF-8" />
		<title>数据加密表单</title>
		<link href="../../asset/theme/default.css" type="text/css" rel="stylesheet" />
		<link href="../../asset/theme/<?php echo(THEME);?>/css/global.css" type="text/css" rel="stylesheet" />
	</head>
	<body>
		<div class="cmpt-workGround-header">
			<h1>表单数据加密演示</h1>
		</div>
		<div class="cmpt-dataInput-formPanel">
			<div class="formPanel-header">
				<h1><span class="icon-form"></span>表单</h1>
			</div>
			<form id="Form" method="POST" action="../../server/post_crypt.php">
				<!--<div class="formPanel-serverErrorMsg">
						<p>服务器正在崩溃，请稍等。</p>
						<p>某个字段不合法，请联系高飞虎仙僧。</p>
					</div>-->
				<div class="formPanel-body">
					<table>
						<tr>
							<th width="150">卡号：</th>
							<td>
								<input class="field-text"
									required="required"
									autocomplete="off"
									type="text"
									name="card_no"/>
							</td>
						</tr>
						<tr>
							<th>身份证号：</th>
							<td>
								<input class="field-text"autocomplete="off" required="required" type="text" name="user_id"/>
							</td>
						</tr>
					</table>
				</div>
				<div class="formPanel-footer">
					<span id="Pass"
						  class="button button-positive"
						  data-verify="verify">加密提交</span>
				</div>
			</form>
		</div>
		<script src="../../asset/lib/jquery.js"></script>
		<script src="../../asset/lib/sec/rsa.js"></script>
		<script src="../../asset/lib/sec/aes.js"></script>
		<script src="../../asset/lib/sec/md5.js"></script>
		<script src="../../asset/lib/sec/zero_padding.js"></script>
		<script src="../../asset/js/admin.mini.js" ></script>
		<script>
				function getRandomKey(){
					var arrMeta = ['a','b','c','e','f','g','h','i','j','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','2','3','4','5','6','7','8','9','10','11','12','~','@','#','$','%','^','&','*'];
					var intMax = arrMeta.length;
					var arrResult = [];
					for(var i= 0;i<10;i++){
						arrResult.push(arrMeta[parseInt(Math.random()*intMax, 10)]);
					}
					return arrResult.join('');
				}

				function keyEncrypt(pStrValue) {
					var publickey = "b74420f5a4d9abfd2072c9d936dd53e2de2aa790822ad1608807bda3e176b335c51902ca2177824198181ce8bea85de132aaea1104fd043e4ad2c0af705bda966b5d2f92a6ab5170d161eb1e8f7a6b1d5fba673f8a4dcebe55407ef9707782c91b17527af820a2c3a3b586341ae54ef03739074d4738e3ff35257bdfb9233c53";
					var rsakey = new RSAKey();
					rsakey.setPublic(publickey, "10001");
					return rsakey.encrypt(pStrValue);
				}

				var key_hash = CryptoJS.MD5(getRandomKey());
				var key = CryptoJS.enc.Utf8.parse(key_hash);
				var iv  = CryptoJS.enc.Utf8.parse('1234567812345678');
				var elmForm = jQuery('#Form');
				var df = new DataForm(elmForm, jQuery('#Pass'), new FormValidator(elmForm), {
					onBeforeSubmit: function(){
						jQuery.each(elmForm.find('[name]'), function(index, field){
							var encrypted = CryptoJS.AES.encrypt(encodeURIComponent(field.value), key, { iv: iv,mode:CryptoJS.mode.CBC,padding:CryptoJS.pad.ZeroPadding});
							field.value = encrypted;
						});
						// 这里必须用toString()方法输出MD5生成的字符串，否则只是对MD5对象进行RSA加密
						elmForm.append('<input type="hidden" name="key" value="'+keyEncrypt(key_hash.toString())+'" />');
						return true;
					}
				});




		</script>
	</body>
</html>