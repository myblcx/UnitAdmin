<?php
require_once("../../server/conf.php");
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Admin</title>
	<link href="../../asset/theme/default.css" type="text/css" rel="stylesheet" />
	<link href="../../asset/theme/<?php echo(THEME);?>/css/global.css" type="text/css" rel="stylesheet" />
</head>
<body>
<div class="cmpt-workGround-header">
	<h1>表单演示</h1>
</div>
<div class="cmpt-dataInput-formPanel">
	<div class="formPanel-header">
		<h1><span class="icon-form"></span>表单</h1>
	</div>
	<form id="Form" method="POST" action="../../server/post_common.php">
	<div class="formPanel-serverErrorMsg">
		<p>服务器正在崩溃，请稍等。</p>
		<p>某个字段不合法，请联系高飞虎仙僧。</p>
	</div>
	<div class="formPanel-body">
		<table>
			<tr>
				<th width="150"><b class="require"></b>ajax可选验证：</th>
				<td>
					<input class="field-text"
						ajax_verify="../../server/ajax_common.php"
						type="text"
						illegal="@,#,$,%,^,&,*"
						required="required"
						minlength="2"
						maxlength="5"
						name="merchant"/>
					<span role="helper"
						data-help_msg="如果商户名称一样,请确认所选商户号是否正确！"
						class="icon-help"></span>
				</td>
			</tr>
			<tr>
				<th><b class="require"></b>弹窗选择：</th>
				<td>
					<input required="required"
						id="DetailsInformation"
						name="merPaper"
						class="field-text"
						type="text" />
					<span id="GetDetails"
						  data-url="repo/template/dialog.pickup_list.php"
						  class="button button-ordinary">查询</span>
				</td>
			</tr>
			<tr>
				<th>自动填入的字段：</th>
				<td>
					<input type="text"
						id="Details2"
						name="pink"
						class="field-text" />
				</td>
			</tr>
			<tr>
				<th>卡号格式化：</th>
				<td>
					<input type="text"
						id="CardNo"
						name="card"
						class="field-text" />
				</td>
			</tr>
			<tr>
				<th>可选项：</th>
				<td>
					<input name="alon" class="field-text" type="text" />
				</td>
			</tr>
			<tr>
				<th>自定义验证器：</th>
				<td>
					<input name="yinye"
						class="field-text"
						type="text" />
				</td>
			</tr>
			<tr>
				<th>自定义长度：</th>
				<td>
					<input class="field-text grid_9" type="text" />
				</td>
			</tr>
			<tr>
				<th><b class="require"></b>级联菜单：</th>
				<td>
					<select name="galaxy"
						data-cmgroup="E"
						class="field-select"
						role="cascadeAjaxMenu"
						data-url="../../server/ajax_cascade_menu.php">
						<option value="1">安德洛星系</option>
						<option value="2">德哈星系</option>
					</select>
					<select name="constellation"
						data-cmgroup="E"
						class="field-select"
						role="cascadeAjaxMenu"
						data-url="../../server/ajax_cascade_menu.php">
						<option value="-1">--请选择--</option>
						<option value="1">科普路星区</option>
						<option value="2">依翰星区</option>
					</select>
					<select name="planet"
						disabled="disabled"
						class="field-select"
						data-cmgroup="E"
						role="cascadeAjaxMenu"
						data-url="../../server/ajax_cascade_menu.php"
						disabled="disabled"></select>
				</td>
			</tr>
			<tr>
				<th class="top">大文本框：</th>
				<td>
					<textarea name="evea" cols="30" rows="6" data-illegal="\r,\n"></textarea>
				</td>
			</tr>
			<tr>
				<th><b class="require"></b>缺省下拉菜单：</th>
				<td>
					<select name="card" required="required" class="field-select">
						<option value="-1">--请选择--</option>
						<option value="1" selected="selected">信用卡</option>
						<option value="2">借记卡</option>
					</select>
				</td>
			</tr>
		</table>
	</div>
	<div class="formPanel-footer">
		<span id="Back" class="button button-ordinary">返回</span>
		<span id="Pass"
			class="button button-positive"
			data-verify="verify"
			data-action="../../server/post_common.php">通过审核</span>
		<span id="Reject"
			data-verify="verify"
			class="button button-negative"
			data-prompt="请填写驳回理由"
			data-action="../../server/post_reject.php">驳回</span>
	</div>
	</form>
</div>
<script src="../../asset/lib/jquery.js"></script>
<script src="../../asset/js/admin.mini.js"></script>
<script>

		var elmForm = jQuery('#Form'),
				elmSubmits = jQuery('#Pass,#Reject');
		var fv = new FormValidator(elmForm);
		// 自定义验证器
		fv.customValidator({
			'yinye': [{
				errMsg: '数据不能是xx',
				verify: function(elmField){
					return elmField.val() != 'xx';
				}
			}, {
				errMsg: '数据不能是oo',
				verify: function(elmField){
					return elmField.val() != 'oo';
				}
			}]
		});

		// 提交表单
		var df = new DataForm(elmForm, elmSubmits, fv);

		// 表单域帮助信息
		jQuery('[role="helper"]').on('click', function(){
			alert(jQuery(this).attr('data-help_msg'));
		});

		// 级联菜单
		var cam = new CascadeAjaxMenu(elmForm.find('[data-cmgroup="E"]'));

		// 后退
		jQuery('#Back').on('click', function(){
			PreloadingPage.beginLoad();
			window.history.back();
		});

		// 选取条目
		var elmGetDetails = jQuery('#GetDetails'),
				elmDetailsInfo1 = jQuery('#DetailsInformation'),
				elmDetailsInfo2 = jQuery('#Details2');
		window.popupPicker = new PopupPicker(elmGetDetails,
				[elmDetailsInfo1, elmDetailsInfo2]);

		// 卡号格式化
		var elmCardNo = jQuery('#CardNo'),
				elmPreviewPanel = jQuery('<div class="cmpt-formatPreview"></div>'),
				elmSlave;
		elmCardNo.on('input', function(){
			elmSlave.html(TextFormat.cardNo(elmCardNo.val()));
		}).on('focus', function(){
					elmSlave = elmPreviewPanel.clone();
					elmBody.append(elmSlave);
					elmSlave.html(TextFormat.cardNo(elmCardNo.val()));
					elmSlave.css({
						left: elmCardNo.offset().left,
						top: elmCardNo.offset().top - 36
					});
				}).on('blur', function(){
					elmSlave.remove();
				});

</script>
</body>
</html>