<?php
require_once("../../server/conf.php");
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Admin</title>
	<link href="../../asset/theme/default.css" type="text/css" rel="stylesheet" />
	<link href="../../asset/theme/<?php echo(THEME);?>/css/global.css" type="text/css" rel="stylesheet" />
</head>
<body>

<div class="cmpt-workGround-header">
	<h1>数据列表演示</h1>
</div>
<div class="cmpt-dataOutput-table" id="DataOutput">
	<div class="dataTable-header">
		<h1><span class="icon-grid"></span>数据表</h1>
		<div class="dataTable-topOperations">
			<a role="switchWorkGround" href="data_input_form.php" id="Add" class="operation" title="新增数据">
				<span class="icon-add"></span>
			</a>
			<div role="advConditions"
				data-url="repo/template/dialog.adv_conds.php"
				class="operation"
				title="全部搜索条件">
				<span class="icon-more"></span>
			</div>
		</div>
	</div>
	<!--关键字-->
	<div class="dataTable-keywords">
		<ul class="clearfix">
			<li><span class="keyword">关键字1</span><span title="移除关键字" role="removeKeyword" data-name="p1" class="icon-remove"></span></li>
			<li><span class="keyword">关键字2</span><span title="移除关键字" role="removeKeyword" data-name="p2" class="icon-remove"></span></li>
			<li><span class="keyword">关键字3</span><span title="移除关键字" role="removeKeyword" data-name="p3" class="icon-remove"></span></li>
			<li><span class="keyword">关键字4</span><span title="移除关键字" role="removeKeyword" data-name="p4" class="icon-remove"></span></li>
		</ul>
	</div>
	<!--简单条件查询-->
	<form role="queryForm" action="#" method="GET">
	<div class="dataTable-simpleFilter">
		<p>
			<input class="field-text grid_5" type="text" placeholder="商户名称" name="reqColumnValue" />
			<span role="querySubmit" class="button button-query">查询</span>
		</p>
	</div>
		<input type="hidden" name="p1" value="关键字1" />
		<input type="hidden" name="p2" value="关键字2" />
		<input type="hidden" name="p3" value="关键字3" />
		<input type="hidden" name="p4" value="关键字4" />
	</form>
	<!--<div class="dataTable-body fullWidth">-->
	<div class="dataTable-body">
		<table class="dataList" role="dataList">
			<thead>
			<tr>
				<th class="grid_1"><span></span></th>
				<th><span>商户名</span></th>
				<th><span>商户地址</span></th>
				<th><span>操作</span></th>
				<th><span>&nbsp;</span></th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td></td>
				<td>樱花日语</td>
				<td>上海人民广场培训中心</td>
				<td>
					<span role="delete"
						data-id="01"
						data-msg="上海人民广场培训中心"
						data-url="../../server/post_common.php"
						class="button button-negative">删除</span>
					<span role="prompt"
						data-msg="请填写驳回理由"
						data-url="../../server/post_common.php"
						class="button button-negative">驳回</span>
				</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td></td>
				<td>樱花日语</td>
				<td>上海浦东八佰伴培训中心</td>
				<td>
					<span role="delete"
						data-id="02"
						data-msg="麦特鲁克威尔"
						data-url="../../server/post_common.php"
						class="button button-negative">删除</span>
				</td>
				<td>&nbsp;</td>
			</tr>
			</tbody>
		</table>
	</div>
	<div class="dataTable-footer clearfix">
		<div role="pagination" class="pagination">
			<a class="disabled">&lt;</a>
			<a href="./data_output_list.php?page_num=1">1</a>
			<span>...</span>
			<a href="./data_output_list.php?page_num=11">11</a>
			<a class="selected">12</a>
			<a href="./data_output_list.php?page_num=13">13</a>
			<span>...</span>
			<a href="./data_output_list.php?page_num=23">23</a>
			<a href="./data_output_list.php?page_num=13">&gt;</a>
			<select>
				<option>第1页</option>
				<option>第2页</option>
				<option>第3页</option>
				<option>第4页</option>
				<option>第5页</option>
				<option>第6页</option>
				<option>第7页</option>
				<option>第8页</option>
				<option>第9页</option>
				<option>第10页</option>
				<option>第11页</option>
				<option selected="selected">第12页</option>
				<option>第13页</option>
				<option>第14页</option>
				<option>第15页</option>
				<option>第16页</option>
				<option>第17页</option>
				<option>第18页</option>
				<option>第19页</option>
				<option>第20页</option>
				<option>第21页</option>
				<option>第22页</option>
				<option>第23页</option>
			</select>
		</div>
	</div>

</div>

<script src="../../asset/lib/jquery.js"></script>
<script src="../../asset/js/admin.mini.js"></script>
<script>
		var elmDataOutput = jQuery('#DataOutput');
		// 数据表格渲染
		var dl = new DataList(elmDataOutput);
		// 新增商户
		jQuery('#Add').on('click', function(){
			PreloadingPage.beginLoad();
			return true;
		});
		// 禁止某些地方被复制
		ClientPermission.copyForbidden(elmDataOutput.find('[role="dataList"]'));

</script>
</body>
</html>