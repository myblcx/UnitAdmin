<?php
require_once("../../server/conf.php");
?>
<!DOCTYPE html>
<html>
<head>
	<title>All Filters</title>
	<link href="../../asset/theme/default.css" type="text/css" rel="stylesheet" />
	<link href="../../asset/theme/<?php echo(THEME);?>/css/global.css" type="text/css" rel="stylesheet" />
	<link href="../../asset/theme/<?php echo(THEME);?>/css/dialog.css" type="text/css" rel="stylesheet" />
</head>
<body class="page-dialog">
	<div class="cmpt-dataInput-formPanel cmpt-dataInput-allConditions">
		<form action="./data_input_form.php" target="work_ground" id="AdvQueryForm">
		<div class="formPanel-body">
			<table>
				<tr>
					<th width="120">商户名：</th>
					<td><input name="cond-1" class="field-text" /></td>
				</tr>
				<tr>
					<th>商户编号：</th>
					<td><input name="cond-2" class="field-text" /></td>
				</tr>
				<tr>
					<th>终端ID：</th>
					<td>
						<select name="cond-3">
							<option>--请选择--</option>
							<option>00101</option>
							<option>00102</option>
						</select>
					</td>
				</tr>
			</table>
		</div>
		<div class="formPanel-footer">
			<span id="SubmitAdvQuery" class="button button-query">查询</span>
		</div>
		</form>
	</div>
	<script src="../../asset/lib/jquery.js"></script>
	<script>
		jQuery('#SubmitAdvQuery').on('click', function(){
			jQuery('#AdvQueryForm').submit();
			window.parent.closeIfmDialog();
		});
	</script>
</body>
</html>