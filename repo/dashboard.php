<?php
require_once("../server/conf.php");
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Dashboard</title>
	<link href="../asset/theme/default.css" type="text/css" rel="stylesheet" />
	<link href="../asset/theme/<?php echo(THEME);?>/css/global.css" type="text/css" rel="stylesheet" />
</head>
<body>
<div class="cmpt-workGround-header">
	<h1>欢迎使用华势作业平台系统</h1>
</div>
<div class="cmpt-dataOutput-table" id="DataOutput">
	<div class="dataTable-header">
		<h1><span class="icon-grid"></span>系统公告</h1>
	</div>
	<div class="dataTable-body">
		<table class="dataList" role="dataList">
			<thead>
			<tr>
				<th class="grid_4"><span>日期</span></th>
				<th><span>说明</span></th>
				<th><span>&nbsp;</span></th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td>2013/01/03 15:13</td>
				<td>系统宕机</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>2013/01/03 15:13</td>
				<td>数据库宕机</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>2013/01/03 15:13</td>
				<td>WEB服务器宕机</td>
				<td>&nbsp;</td>
			</tr>
			</tbody>
		</table>
	</div>
	<div class="dataTable-footer clearfix">
		<div role="pagination" class="pagination">
			<a class="disabled">&lt;</a>
			<a href="./dashboard.php?page_num=1">1</a>
			<span>...</span>
			<a href="./dashboard.php?page_num=11">11</a>
			<a class="selected">12</a>
			<a href="./dashboard.php?page_num=13">13</a>
			<span>...</span>
			<a href="./dashboard.php?page_num=23">23</a>
			<a href="./dashboard.php?page_num=13">&gt;</a>
			<select>
				<option>第1页</option>
				<option>第2页</option>
			</select>
		</div>
	</div>
</div>

<script src="../asset/lib/jquery.js"></script>
<script src="../asset/js/admin.mini.js"></script>
<script>

	var dl = new DataList(jQuery('#DataOutput'));

</script>
</body>
</html>